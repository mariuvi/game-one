

// Settings
var max = 100,
    score = 0;

// Elements
var water = document.createElement('div');
water.classList.add('water');

var lake = document.querySelector('.lake');
var bucket;


// Functions
function getRandomArbitrary(min, max) {
    return Math.ceil( Math.random() * (max - min) + min);
}

function createLake(size) {
    for (var i = 1; i <= size; i++) {
        bucket = water.cloneNode(true);
        lake.appendChild(bucket);
    }
}

function createDucks(quantity, context){
    var elements = context.children;
    var index;

    for (var i = 1; i <= quantity; i++){
    index = getRandomArbitrary(0, elements.length - 1);
    elements[index].classList.add('duck');
    elements[index].addEventListener('click', shoot);
    }
}

// function createDuck(element) {
//     if (!element.classList.contains(duck))
// }

function shoot(event) {
    var target = event.target;
    target.classList.add('dead');
    score++;
    this.removeEventListener('click', shoot);
    document.querySelector('.score').innerHTML = score;


    // this?
    // change visual
    // change score
}

// Flow
createLake(max);
createDucks(max / 10, lake);